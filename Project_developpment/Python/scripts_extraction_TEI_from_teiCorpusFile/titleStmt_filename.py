# Division d'un fichier XML teiCorpus en plusieurs fichier TEI
# Récupération de la valeur de l'élément <title> de l'élément <titleStmt> pour nommer les fichiers XML créés
from xml.dom import minidom

dom = minidom.parse('test_extraction_TEI_in_teiCorpus.xml')
sections = dom.getElementsByTagName('TEI')
for section in sections : 
    titleStmts = section.getElementsByTagName('titleStmt')
    #print(titleStmts)
    for titleStmt in titleStmts :
        titles = titleStmt.getElementsByTagName('title')
        #print(titles)
        for title in titles :
            title_file = title.firstChild.data
            #print(title_file)
            open(title_file + ".xml", 'w').write(section.toprettyxml())