# Division d'un fichier XML teiCorpus en plusieurs fichier TEI
# Récupération des valeurs adéquates pour nommer les fichiers XML créés selon la procédure suivante : emetteur_à_destinataire_date_envoi. Selon qualité de l'OCR et de la structuration semi-automatisée de l'encodage, besoin d'harmoniser manuellement certains titres. Limite du remplissage de ces champs. 
from xml.dom import minidom

dom = minidom.parse('Walpole_Toynbee_1.xml')
sections = dom.getElementsByTagName('TEI')
for section in sections : 
    correspActions = section.getElementsByTagName('correspAction')
    #print(correspActions)
    for correspAction in correspActions :
        if correspAction.getAttribute('type') == "sent":
            titles_part1 = correspAction.getElementsByTagName('persName')
            #print(titles_part1)
            titles_part3 = correspAction.getElementsByTagName('date')
            #print(titles_part3)
        if correspAction.getAttribute('type') == "received":
            titles_part2 = correspAction.getElementsByTagName('persName')
            #print(titles_part2)
        for title_part1 in titles_part1 :
            title_part1 = title_part1.firstChild.data
            #print(title_part1)
        for title_part2 in titles_part2 :
            title_part2 = title_part2.firstChild.data
            #print(title_part2)
        for title_part3 in titles_part3 :
            #if title_part3 is False :
                #pass
            #if title_part3 is True :
            try :
                title_part3 = title_part3.firstChild.data
                #print(title_part3)
            except :
                title_part3 = "??"
                #print("Absence de date pour cette lettre.")
            
        title_file = title_part1.replace(" ", "_") + title_part2.replace(" ", "_") + "_" + title_part3.replace(" ", "_")
        #print(title_file)
        open(title_file + ".xml", 'w').write(section.toprettyxml())