<?xml version="1.0" encoding="UTF-8"?>
<!-- 1ère passe à appliquer sur le fichier océrisé stylisé : délimitation de la structure avec une balise TEI par lettre, un header et la récupération du texte. -->

<!-- On inclut la déclaration non prefixé de TEI en déclaration d'espace de nom par défaut pour ne ne pas avoir à la répéter dans chaque nouvelle
création de balise TEI. -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" 
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="3.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <!-- Suppression des espaces blancs en trop pour tous les éléments. -->
    <xsl:strip-space elements="*"/>
    <xsl:output indent="yes" method="xml" encoding="UTF-8"/>
    
    <!-- Création de l'élément racine et de son premier descendant. -->
    <xsl:template match="TEI">
        <xsl:element name="teiCorpus" namespace="http://www.tei-c.org/ns/1.0">
            <!-- Création de l'arborescence TEI par lettre et récupération du texte stylisé -->
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <!-- teiHeader du teiCorpus à copier-coller du modèle après transformation -->
    
    <!-- Création de l'arborescence TEI par lettre et récupération du texte stylisé -->
    <xsl:template match="p[starts-with(., '*')]">
        <xsl:variable name="letter-id" select="generate-id(.)"/>
        <xsl:for-each select=".">
            <xsl:element name="TEI">
                <xsl:element name="teiHeader">
                    <xsl:element name="fileDesc">
                        <xsl:element name="titleStmt">
                            <xsl:element name="title">
                                Titre de la lettre : noms correspondants + date + incipit
                            </xsl:element>
                            <xsl:element name="respStmt">
                                <xsl:element name="resp">Encodage</xsl:element>
                                <xsl:element name="persName">
                                    <xsl:attribute name="ref">#id_personne</xsl:attribute>
                                    Nom responsable</xsl:element>
                            </xsl:element>
                            <xsl:element name="respStmt">
                                <xsl:element name="resp">Vérification encodage technique</xsl:element>
                                <xsl:element name="persName">
                                    <xsl:attribute name="ref">#GP</xsl:attribute>
                                    Gwenaëlle Patat</xsl:element>
                            </xsl:element>
                            <xsl:element name="respStmt">
                                <xsl:element name="resp">Vérification encodage scientifique</xsl:element>
                                <xsl:element name="persName">
                                    <xsl:attribute name="ref">#MCV</xsl:attribute>
                                    Nom responsable</xsl:element>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="publicationStmt">
                            <xsl:element name="publisher">Université Bretagne Occidentale, CECJI</xsl:element>
                            <xsl:element name="publisher">Université Bretagne Sud</xsl:element>
                            <xsl:element name="publisher">Université de Rennes 1</xsl:element>
                            <xsl:element name="publisher">Université Sorbonne-Nouvelle Paris 3</xsl:element>
                            <xsl:element name="publisher">Université de Téluq, Canada</xsl:element>
                            <xsl:element name="idno">
                                <xsl:attribute name="type">DOI</xsl:attribute>
                                id Nakala</xsl:element>
                            <xsl:element name="date">
                                <xsl:attribute name="when">2022</xsl:attribute>
                            </xsl:element>
                            <xsl:element name="availability">
                                <xsl:attribute name="status">restricted</xsl:attribute>
                                <xsl:element name="licence">
                                    <xsl:attribute name="target">https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr</xsl:attribute>
                                    Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes
                                    Conditions 4.0 International (CC BY-NC-SA 4.0)</xsl:element>
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="notesStmt">
                            <xsl:element name="note"/>
                        </xsl:element>
                        <xsl:element name="sourceDesc">
                            <xsl:element name="listWit">
                                <xsl:element name="witness"> 
                                    <xsl:attribute name="corresp">#id_source_imprimee</xsl:attribute>
                                    <xsl:element name="biblStruct">
                                        <xsl:element name="monogr">
                                            <xsl:element name="imprint">
                                                <xsl:element name="biblScope">
                                                    <xsl:attribute name="from">n°p</xsl:attribute>
                                                    <xsl:attribute name="to">n°p</xsl:attribute>
                                                    Pages relatives à la lettre dans une édition imprimée
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="witness">
                                    <xsl:attribute name="corresp">#id_manuscrit</xsl:attribute>
                                    <xsl:element name="msDesc">
                                        <xsl:element name="msIdentifier">
                                            <xsl:element name="repository"/>
                                            <!-- Extraction de la côte (péritexte) si placée après <p>***</p>-->
                                            <xsl:element name="idno">
                                                <xsl:value-of select="following-sibling::p[1]"/>
                                            </xsl:element>
                                        </xsl:element>
                                        <xsl:element name="physDesc">
                                            <xsl:element name="objectDesc">
                                                <xsl:attribute name="form">original</xsl:attribute>
                                                <xsl:element name="supportDesc">
                                                    <xsl:attribute name="material"/>
                                                    <xsl:element name="extent">
                                                        <xsl:element name="dimensions">
                                                            <xsl:attribute name="scope"/>
                                                            <xsl:attribute name="type"/>
                                                            <xsl:attribute name="unit"/>
                                                            <xsl:element name="height"/>
                                                            <xsl:element name="width"/>
                                                        </xsl:element>
                                                    </xsl:element>
                                                </xsl:element>
                                            </xsl:element>
                                            <xsl:element name="handDesc">
                                                <xsl:attribute name="hands">nombre de scripteurs</xsl:attribute>
                                                <xsl:element name="p">Scripteurs</xsl:element>
                                            </xsl:element>
                                            <xsl:element name="accMat">
                                                <xsl:element name="label">
                                                    <xsl:attribute name="corresp">#id_lettre</xsl:attribute>
                                                    Lettre indexée (idem sans @corresp pour un objet non indexé)
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element> 
                    </xsl:element> 
                    <xsl:element name="profileDesc">
                        <xsl:element name="correspDesc">
                            <xsl:element name="correspAction">
                                <xsl:attribute name="type">sent</xsl:attribute>
                                <!-- Extraction expéditeur -->
                                <xsl:element name="persName">
                                    <xsl:attribute name="ref">#id_exped</xsl:attribute>
                                    <xsl:value-of select="following-sibling::p[1]/hi[@rend='bold']|following-sibling::p[2]/hi[@rend='bold']|following-sibling::p[3]/hi[@rend='bold']"/>
                                </xsl:element>

                                <!-- Extraction lieu d'envoi (péritexte), si indiqué dans l'un des trois premiers <p> -->
                                <xsl:element name="placeName">
                                    <xsl:attribute name="ref">#id_lieu</xsl:attribute>
                                    <xsl:value-of select="following-sibling::p[1]/hi[@rend='bold underline']|following-sibling::p[2]/hi[@rend='bold underline']|following-sibling::p[3]/hi[@rend='bold underline']"/>
                                </xsl:element>
                                  
                                
                                <!-- Extraction date d'envoi (péritexte), si elle est indiquée dans l'un des trois premiers <p>-->
                                <xsl:element name="date">
                                    <xsl:value-of select="following-sibling::p[1]/hi[@rend='underline']|following-sibling::p[2]/hi[@rend='underline']|following-sibling::p[3]/hi[@rend='underline']"/>
                                </xsl:element>

                            </xsl:element> 
                            <xsl:element name="correspAction">
                                <xsl:attribute name="type">received</xsl:attribute>
                                <!-- Extraction destinataire (péritexte) si indiqué dans l'un des trois premiers <p>-->
                                <persName>
                                    <xsl:attribute name="ref">#id_dest</xsl:attribute>
                                    <xsl:value-of select="following-sibling::p[1]/hi[@rend='italic']|following-sibling::p[2]/hi[@rend='italic']|following-sibling::p[3]/hi[@rend='italic']"/>
                                </persName>
                                <xsl:element name="placeName">
                                    <xsl:attribute name="ref">#id_lieu</xsl:attribute>
                                </xsl:element>
                                <xsl:element name="date">
                                    <!--<xsl:attribute name="when"></xsl:attribute>-->
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="correspContext">
                                <xsl:element name="ref">
                                    <xsl:attribute name="type">prev</xsl:attribute>
                                    <xsl:attribute name="corresp">id_lettre_precedente</xsl:attribute>
                                    Lettre précédente :
                                    <xsl:element name="persName">
                                        <xsl:attribute name="ref">#id_exped_lettre_precedente</xsl:attribute> 
                                    </xsl:element> à
                                    <xsl:element name="persName">
                                        <xsl:attribute name="ref">#id_dest_lettre_precedente</xsl:attribute> 
                                    </xsl:element> le
                                    <xsl:element name="date">
                                        <!--<xsl:attribute name="when"></xsl:attribute>-->
                                    </xsl:element>.
                                </xsl:element>
                                <xsl:element name="ref">
                                    <xsl:attribute name="type">next</xsl:attribute>
                                    <xsl:attribute name="corresp">id_lettre_suivante</xsl:attribute>Lettre suivante :
                                    <xsl:element name="persName">
                                        <xsl:attribute name="ref">#id_exped_lettre_suivante</xsl:attribute> 
                                    </xsl:element> à
                                    <xsl:element name="persName">
                                        <xsl:attribute name="ref">#id_dest_lettre_suivante</xsl:attribute> 
                                    </xsl:element> le
                                    <xsl:element name="date">
                                        <!--<xsl:attribute name="when"></xsl:attribute>-->
                                    </xsl:element>.
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="revisionDesc">
                        <xsl:element name="change">
                            <xsl:attribute name="when"></xsl:attribute>
                            <xsl:attribute name="who">#id_personne</xsl:attribute>
                        </xsl:element>
                    </xsl:element>   
                </xsl:element>
                <xsl:element name="text">
                    <xsl:element name="body">
                        <xsl:element name="div">
                            <xsl:attribute name="type">
                                <xsl:text>modernized_version</xsl:text>
                            </xsl:attribute>
                            <xsl:for-each select="following-sibling::p[generate-id(preceding-sibling::p[starts-with(., '*')][1]) = $letter-id]">
                                <xsl:copy-of select="."/> 
                            </xsl:for-each>
                        </xsl:element>
                        <xsl:element name="div">
                            <xsl:attribute name="type">
                                <xsl:text>diplomatic_version</xsl:text>
                            </xsl:attribute>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
    
    
    <!-- Condition pour ne pas récupérer le texte des lettres entre les balises TEI créées -->
    <xsl:template match="p[not(starts-with(., '*'))]"/>
</xsl:stylesheet>