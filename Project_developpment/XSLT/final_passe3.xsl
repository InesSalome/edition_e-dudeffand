<?xml version="1.0" encoding="UTF-8"?>

<!-- On inclut la déclaration non prefixé de TEI en déclaration d'espace de nom par défaut pour ne ne pas avoir à la répéter dans chaque nouvelle
création de balise TEI. -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" 
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="3.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <!-- Suppression des espaces blancs en trop pour tous les éléments. -->
    <xsl:strip-space elements="*"/>
    <xsl:output indent="yes" method="xml" encoding="UTF-8"/>
    
    <!-- Copy source file : Ce bout de code permet de ne pas perdre d'informations lors de la transformation en copiant tous les nœuds du document source.-->
    <xsl:template match="@* | node()" mode="#all">
        <xsl:choose>
            <xsl:when test="matches(name(.), '^(part|instant|anchored|default|full|status)$')"/>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@* | node()" mode="#current"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

<!-- Passe 3 : organisation du contenu du <div> -->
    
    <xsl:template match="//text/body/div">
        
        <!-- On crée l'élément <div> avec son attribut -->
        <xsl:element name="div">
            <xsl:attribute name="type">
                <xsl:value-of select="@type"/>
            </xsl:attribute>
            
            <!-- création du <opener> dans lequel on insère le salute et la dateline -->
        <xsl:element name="opener">
            <xsl:copy-of select="dateline[@n='opener']|salute[@n='opener']"/>
        </xsl:element>
            
            <!-- On récupère le corps du texte en excluante <p>***<p> et le paratexte-->
            <!-- Attention : la taille du péritexte diffère d'une édition à l'autre, il faut donc adapter le prédicat en fonction de la position des <p> que l'on souhaite supprimer. -->
            <!-- On pourrait également faire une quatrième passe en supprimant les <p> placés en amont du <opener> -->
            <xsl:copy-of select="p[position() !=[1] and position() !=[2]] except p[starts-with(.,'*')]|l|date"/>
           
        
            <!-- création du <closer> -->
        <xsl:element name="closer">
            <xsl:copy-of select="signed|salute[@n='closer']|dateline[@n='closer']"/>
        </xsl:element>
            
            <!-- <postscript> : risque de poser problème si jamais la lettre est signée après le P.S. -->
            <xsl:copy-of select="postscript"/> 
        </xsl:element>
    </xsl:template>
    
</xsl:stylesheet>