# e.dudeffand project : Digital edition of Mme Du Deffand's complete correspondence

This repository includes data, documentation and source code from the project e.dudeffand supported by the Maison des Sciences de l'Homme en Bretagne. Mme du Deffand is a major letter writer of the Enlightenment. The 1450 letters she corresponded with famous people have aroused the interest of many researchers on a national and international scale. The participants in this project are particularly interested in the social impact of illness and old age, the place and role of women in public life, and the construction of epistolary networks on a European scale. Our interdisciplinary project, conducted with Geoffrey Williams (UBS), Melinda Caron (Université TÉLUQ, Montréal), Benedicte Peralez-Peslier (Université Paris 3), Marianne Charrier-Vozel (Université Rennes 1 and UBO) and Marine Parra (UBS), is based on the observation that epistolary work to date has too often followed an editorial division. From 1809 to 2013, we count 26 editions of the correspondence grouped according to the recipients ; some promise to be complete, albeit more letters continue to come to light thereby underlining the need for an evolutive edition using digital means. At the end of the two years of the MSHB project, our objective is twofold : to build and make available, according to the FAIR principles, an interface for consulting the digital XML-TEI compliant edition of the letters free of rights and to contact with holding institutions in order to obtain the rights for all the correspondence found on this date. This interface will be common to all familiar correspondence and will allow cross-searching of a corpus that could amount to thousands of letters.

# Structure of the repository

- Documentation
    - OCR_Process
    - Styles_for_encoding
    - semi_automation_encoding

- Project_developpment
    - Python
    - XSLT

Letters from the corpus will be progressively accessible in the collection dedicated to the project in data repository Nakala.
